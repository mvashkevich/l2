﻿using System;
using FakerPluginBase;

namespace FakerPlugins
{
    public class DoubleFaker : IFaker
    {
        private readonly Random _random = new Random();

        Type IFaker.GetFakedType => typeof(double);

        public object Generate()
        {
            return _random.NextDouble();
        }
    }
}
