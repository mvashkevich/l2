﻿using System;
using FakerPluginBase;

namespace FakerPlugins
{
    class FloatFaker : IFaker
    {
        private readonly Random _random = new Random();

        Type IFaker.GetFakedType => typeof(float);

        public object Generate()
        {
            return (float)_random.NextDouble();
        }
    }
}
