﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FakerPluginBase;

namespace FakerLib.Implementations
{
    class StringFaker : IFaker
    {
        private static readonly string[] Nouns = { "I", "He", "She", "Flower", "They", "Butterflies" };
        private static readonly string[] Verbs = { "am", "is", "are" };
        private static readonly string[] Adjectives = { "beautiful", "charming", "amazing" };
        private static readonly string[] PunctuationMarks = { ".", "!" };
        private static readonly string[][] SentenceParts = { Nouns, Verbs, Adjectives, PunctuationMarks };

        private readonly Random _random = new Random();
        public Type GetFakedType => typeof(string);

        public object Generate()
        {
            string result = "";
            foreach (string[] sentencePart in SentenceParts)
            {
                result += sentencePart[_random.Next() % sentencePart.Length] + ' ';
            }
            return result;
        }
    }
}
