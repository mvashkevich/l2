﻿using System;
using FakerPluginBase;

namespace FakerLib.Implementations
{
    class LongFaker : IFaker
    {
        private static Random _random = new Random();

        Type IFaker.GetFakedType => typeof(long);

        public object Generate()
        {
            byte[] buffer = new byte[sizeof(long)];
            _random.NextBytes(buffer);

            return BitConverter.ToInt64(buffer, 0);
        }
    }
}
