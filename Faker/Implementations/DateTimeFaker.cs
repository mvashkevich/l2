﻿using System;
using FakerPluginBase;

namespace FakerLib.Implementations
{
    class DateTimeFaker : IFaker
    {
        public Type GetFakedType => typeof(DateTime);

        public object Generate()
        {
            return DateTime.Now;
        }
    }
}
