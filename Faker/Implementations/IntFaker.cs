﻿using System;
using FakerPluginBase;

namespace FakerLib.Implementations
{
    class IntFaker : IFaker
    {
        private static Random _random = new Random();

        Type IFaker.GetFakedType => typeof(int);

        public object Generate()
        {
            return _random.Next();
        }
    }
}
