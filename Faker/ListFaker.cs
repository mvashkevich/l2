﻿using System;
using System.Collections.Generic;

namespace FakerLib
{
    class ListFaker
    {
        private static readonly Random _random = new Random();
        private const int MaxListSize = 10;
        public static IList<T> Generate<T>(Faker faker)
        {
            int size = _random.Next(MaxListSize);
            IList<T> result = new List<T>(size);
            for (int i = 0; i < size; ++i)
            {
                result.Add(faker.Create<T>());
            }
            return result;
        }
    }
}
