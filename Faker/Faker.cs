﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using FakerLib.Implementations;
using FakerPluginBase;
using System.Reflection;


namespace FakerLib
{
    public sealed class Faker
    {
        private const string PluginsPath = @".\plugins\";
        private readonly Dictionary<Type, IFaker> _fakerImplementations = new Dictionary<Type, IFaker>();
        private readonly FakerConfig _config;
        public Faker()
        {
            _config = new FakerConfig();
            _fakerImplementations.Add(typeof(int), new IntFaker());
            _fakerImplementations.Add(typeof(long), new LongFaker());
            _fakerImplementations.Add(typeof(string), new StringFaker());
            _fakerImplementations.Add(typeof(DateTime), new DateTimeFaker());
            foreach (IFaker plugin in PluginLoader.Load<IFaker>(PluginsPath))
            {
                _fakerImplementations.Add(plugin.GetFakedType, plugin);
            }
        }

        public Faker(FakerConfig config) : this()
        {
            if (config != null)
            {
                _config = config;
            }
        }

        private T Generate<T>(HashSet<Type> visitedTypes)
        {
            T result = default(T);
            var type = typeof(T);
            if (typeof(IList).IsAssignableFrom(type))
            {
                result = GenerateList<T>(type);
            }
            else if (_fakerImplementations.ContainsKey(type))
            {
                result = GenerateBaseType<T>(type);
            }
            else if (type.IsClass && !visitedTypes.Contains(type))
            {
                result = GenerateDTO<T>(visitedTypes, type);
            }
            return result;
        }

        private T GenerateDTO<T>(HashSet<Type> visitedTypes, Type type)
        {
            visitedTypes.Add(type);
            T result = default(T);
            FieldInfo[] publicFields;
            IEnumerable<PropertyInfo> propertiesWithPublicSetter;
            ConstructorInfo[] constructors = type.GetConstructors();
            if (constructors.Length > 0)
            {
                int constructorIndex = GetConstructorIndex(type, out publicFields, out propertiesWithPublicSetter, constructors);

                var constructor = constructors[constructorIndex];
                var constructorParameters = constructor.GetParameters();

                var generatedMembers = new HashSet<string>();
                T constructedObject = ConstructObject<T>(visitedTypes, constructor, constructorParameters, generatedMembers);
                SetProperties(visitedTypes, propertiesWithPublicSetter, generatedMembers, constructedObject);
                SetFields(visitedTypes, publicFields, generatedMembers, constructedObject);
                result = constructedObject;
            }
            return result;
        }

        private object GenerateMember(HashSet<Type> visitedTypes, Type type, string name, Type objectType)
        {
            var generators = _config.GetGenerators(objectType);
            object result;
            var generatorKey = new Tuple<Type, string>(type, name);
            if (generators.ContainsKey(generatorKey))
            {
                var generator = generators[generatorKey];
                result = generator.Generate();
            }
            else
            {
                result = typeof(Faker).GetMethod("Generate", BindingFlags.Instance | BindingFlags.NonPublic)
                    .MakeGenericMethod(type)
                    .Invoke(this, new object[] { visitedTypes });
            }
            return result;
        }

        private void SetFields<T>(HashSet<Type> visitedTypes, FieldInfo[] publicFields, HashSet<string> generatedMembers, T constructedObject)
        {
            foreach (FieldInfo fieldInfo in publicFields)
            {
                if (!generatedMembers.Contains(fieldInfo.Name))
                {
                    object fieldValue = GenerateMember(visitedTypes, fieldInfo.FieldType, fieldInfo.Name, fieldInfo.ReflectedType);
                    fieldInfo.SetValue(constructedObject, fieldValue);
                    generatedMembers.Add(fieldInfo.Name);
                }
            }
        }

        private void SetProperties<T>(HashSet<Type> visitedTypes, IEnumerable<PropertyInfo> propertiesWithPublicSetter, HashSet<string> generatedMembers, T constructedObject)
        {
            foreach (PropertyInfo propertyInfo in propertiesWithPublicSetter)
            {
                if (!generatedMembers.Contains(propertyInfo.Name))
                {
                    object propertyValue = GenerateMember(visitedTypes, propertyInfo.PropertyType, propertyInfo.Name, propertyInfo.ReflectedType);
                    propertyInfo.SetValue(constructedObject, propertyValue);
                    generatedMembers.Add(propertyInfo.Name);
                }
            }
        }

        private T ConstructObject<T>(HashSet<Type> visitedTypes, ConstructorInfo constructor, ParameterInfo[] constructorParameters, HashSet<string> generatedMembers)
        {
            object[] parameters = new object[constructorParameters.Length];
            for (int i = 0; i < constructorParameters.Length; ++i)
            {
                parameters[i] = GenerateMember(visitedTypes, constructorParameters[i].ParameterType, constructorParameters[i].Name, typeof(T));
                generatedMembers.Add(constructorParameters[i].Name);
            }
            T constructedObject = (T)constructor.Invoke(parameters);
            return constructedObject;
        }

        private int GetConstructorIndex(Type type, out FieldInfo[] publicFields, out IEnumerable<PropertyInfo> propertiesWithPublicSetter, ConstructorInfo[] constructors)
        {
            publicFields = type.GetFields();
            var nonPublicFields = type.GetFields(BindingFlags.NonPublic);

            var properties = type.GetProperties();
            var propertiesNonPublicSetter = properties.Where(property => !property.CanWrite || property.GetSetMethod().IsPrivate);
            propertiesWithPublicSetter = properties.Where(property => property.CanWrite && property.GetSetMethod().IsPublic);
            int publicMembersCount = propertiesWithPublicSetter.Count() + publicFields.Count();

            int nonPublicMembersMultiplier = publicMembersCount;

            int constructorIndex = 0;
            int maxConstructorRating = 0;
            for (int i = 0; i < constructors.Length; ++i)
            {
                int publicMembersInCtorCount = 0;
                int nonPublicMembersInCtorCount = 0;
                foreach (ParameterInfo parameterInfo in constructors[i].GetParameters())
                {
                    Func<PropertyInfo, bool> propertyCheck =
                        property => property.Name == parameterInfo.Name && property.PropertyType == parameterInfo.ParameterType;
                    Func<FieldInfo, bool> fieldCheck =
                        field => field.Name == parameterInfo.Name && field.FieldType == parameterInfo.ParameterType;
                    if (publicFields.Any(fieldCheck))
                    {
                        ++publicMembersInCtorCount;
                    }
                    else if (nonPublicFields.Any(fieldCheck))
                    {
                        ++nonPublicMembersInCtorCount;
                    }
                    else if (propertiesWithPublicSetter.Any(propertyCheck))
                    {
                        ++publicMembersCount;
                    }
                    else if (propertiesNonPublicSetter.Any(propertyCheck))
                    {
                        ++nonPublicMembersInCtorCount;
                    }
                }

                int constructorRating = nonPublicMembersInCtorCount * nonPublicMembersMultiplier
                    + publicMembersInCtorCount;

                if (maxConstructorRating < constructorRating)
                {
                    constructorIndex = i;
                    maxConstructorRating = constructorRating;
                }
            }
            return constructorIndex;
        }

        private T GenerateBaseType<T>(Type type)
        {
            T result;
            IFaker faker = _fakerImplementations[type];
            result = (T)faker.Generate();
            return result;
        }

        private T GenerateList<T>(Type type)
        {
            T result;
            var argumentType = type.GenericTypeArguments[0];
            var method = typeof(ListFaker).GetMethod("Generate", BindingFlags.Public | BindingFlags.Static);
            method = method.MakeGenericMethod(argumentType);
            object[] arguments = { this };
            result = (T)method.Invoke(null, arguments);
            return result;
        }

        public T Create<T>()
        {
            HashSet<Type> visitedTypes = new HashSet<Type>();
            return Generate<T>(visitedTypes);
        }
    }
}
