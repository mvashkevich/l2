﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace FakerLib
{
    class PluginLoader
    {
        private const string Pattern = "*.dll";
        public static IEnumerable<T> Load<T>(string DirectoryName) where T : class
        {
            int pluginsCount = 0;
            string[] dlls = Directory.GetFiles(DirectoryName, Pattern);
            foreach (string dll in dlls)
            {
                Assembly assembly = Assembly.LoadFile(Path.GetFullPath(dll));
                
                foreach(Type type in assembly.GetTypes())
                {
                    if (typeof(T).IsAssignableFrom(type))
                    {
                        T result = Activator.CreateInstance(type) as T;
                        if (result != null)
                        {
                            ++pluginsCount;
                            yield return result;
                        }
                    }
                }
            }
                Console.Error.WriteLine(">> Loaded {0} plugins.", pluginsCount);
        }
    }
}
