﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using FakerPluginBase;

namespace FakerLib
{
    public class FakerConfig
    {
        // objectType -> (fieldType, fieldName -> generator)
        private Dictionary<Type, Dictionary<Tuple<Type, string>, IFaker>> _generators;
        private static Dictionary<Tuple<Type, string>, IFaker> EmptyDictionary;

        public FakerConfig()
        {
            _generators = new Dictionary<Type, Dictionary<Tuple<Type, string>, IFaker>>();
            EmptyDictionary = new Dictionary<Tuple<Type, string>, IFaker>();
        }

        public void Add<ObjectType, FieldType, GeneratorType> 
            (Expression<Func<ObjectType, FieldType>> expression) 
            where GeneratorType : IFaker, new()
        {
            if (expression.Body is MemberExpression)
            {
                var memberExpression = ((MemberExpression)expression.Body).Member;

                Type objectType = typeof(ObjectType);
                if (!_generators.ContainsKey(objectType))
                {
                    _generators.Add(objectType, new Dictionary<Tuple<Type, string>, IFaker>());
                }

                _generators[objectType].Add(new Tuple<Type, string>(typeof(FieldType), memberExpression.Name), new GeneratorType());
            }
        }

        internal Dictionary<Tuple<Type, string>, IFaker> GetGenerators(Type objectType)
        {
            if (_generators.ContainsKey(objectType))
            {
                return _generators[objectType];
            }
            return EmptyDictionary;
        }
    }
}
