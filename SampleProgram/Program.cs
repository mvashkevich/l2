﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using FakerLib;

using FakerPluginBase;

namespace SampleProgram
{
    class CityFaker : IFaker
    {
        private static readonly string[] Cities = { "London", "Paris", "Berlin", "New York", "Byelaazersk" };

        private readonly Random _random = new Random();
        public Type GetFakedType => typeof(string);

        public object Generate()
        {
            return Cities[_random.Next() % Cities.Length];
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Expression<Func<DTO1, string>> z = y => y.Name;
            FakerConfig config = new FakerConfig();
            config.Add<DTO1, string, CityFaker>(z);


            Faker faker = new Faker(config);

            Console.Write("List: ");
            PrintList(faker.Create<List<int>>());
            Console.Write("List: ");
            PrintList(faker.Create<List<int>>());
            Console.Write("List: ");
            PrintList(faker.Create<List<int>>());
            Console.WriteLine("Int: {0}", faker.Create<int>());
            Console.WriteLine("Long: {0}", faker.Create<long>());
            Console.WriteLine("Float: {0}", faker.Create<float>());
            Console.WriteLine("Double: {0}", faker.Create<double>());
            Console.WriteLine("String: {0}", faker.Create<string>());

            var m = faker.Create<DTO3>();
            var a = faker.Create<DTO1>();
            var b = faker.Create<DTO2>();
            var c = faker.Create<CycleA>();
            var d = faker.Create<PrivateCtorClass>();
            Console.ReadKey();
        }

        static void PrintList(IList list)
        {
            foreach (var i in list)
            {
                Console.Write(i);
                Console.Write(' ');
            }
            Console.WriteLine();
        }
    }
}