﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleProgram
{
    public class DTO1
    {
        public string Name { get; }
        public int Number { get; set; }

        public double DoubleField;
        private long PrivateLongField;

        public DTO1(string Name, double DoubleField, long PrivateLongField)
        {
            this.Name = Name;
            this.DoubleField = DoubleField;
            this.PrivateLongField = PrivateLongField;
        }

        public DTO1(long PrivateLongField)
        {
            this.PrivateLongField = PrivateLongField;
        }
    }
}
