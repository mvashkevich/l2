﻿using System;

namespace FakerPluginBase
{
    public interface IFaker
    {
        object Generate();
        Type GetFakedType { get; }
    }
}
